#include <iostream>
#include <ctype.h>
#include <cstring>

using namespace std;

int main()
{
    char str[] = "123456789";
    char newStr[100];
    int len , i, j = 0;
    len = strlen(str);

    for (i = len - 1; i >= 0; i--)
    {
        newStr[j++] = str[i];

        if (j % 3 == 0 && i != 0)
            newStr[j++] = ',';
    }
    cout << "Formated number is : " << endl;
    for (i = j - 1; i >= 0; i--)
    {
        cout << newStr[i];
    }

    return 0;
}