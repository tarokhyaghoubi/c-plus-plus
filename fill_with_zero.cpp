/*

    Tarokh (Jacob) Yaghoubi , A program to Fill zero from the 
	left-side of a character array that contains numbers 
	The Buffer must be 12 characters maximum which is the numbers 
	that you inject in the buffer , plus the zeros .
    C++ Programming

*/

#include <iostream>
#include <ctype.h>
#include <cstring>

typedef unsigned char uint8;

using namespace std;

int main(int argc, char* argv[])
{
    int max = 12;
    char zero[13];
    char buffer[12] = "12345675";
    if (strlen(buffer) > 12)
        return -1;
    
    int len = strlen(buffer);
    max = max - len;
    for (int i = 0; i < max; i++)
    {
        zero[i] = '0';
    }
    for (int i = 0, j = max; i < strlen(buffer) && j <= 12; i++, j++)
    {
        zero[j] = buffer[i];
    }
    zero[12] = '\0';

    for (int i = 0; i < 12; i++)
        cout << zero[i];
    
    cout << endl;
    system("pause");
    return 0;
}