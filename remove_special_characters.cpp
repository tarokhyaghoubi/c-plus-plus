/*

    Tarokh (Jacob) Yaghoubi , A program to remove 
    Special Characters at the beggining and the end of 
    a Character Array in C++

*/

#include <iostream>
#include <cstring>
#include <ctype.h>

using namespace std;

int lstrip_special_char(char* ptr);
char* rstrip_special_char(char* ptr);

int main(int argc, char* argv[])
{
    char name[]= "@@##$$ Tarokh is a good boy 45&&^^@@$$#";
    cout << "The name is : " << "{" << name << "}" << endl;
    int len = strlen(name);
    int len_func = lstrip_special_char(name);
    memmove(&name[0], &name[len_func], (len - len_func));
    name[len - len_func] = '\0';
    cout << "The new name is " << "{" << rstrip_special_char(name) << "}" << endl;

    system("pause");
    return 0;
}

int lstrip_special_char(char* ptr)
{
    int i = 0;
    
    while ((*ptr < 65 || *ptr > 90) && (*ptr < 97 || *ptr > 122) && (*ptr < 48 || *ptr > 57))
    {
        i++;
        ptr++;
    }

    return i;
}

char* rstrip_special_char(char* ptr)
{
    char* _end;
    int size = strlen(ptr);
    _end = (size + ptr) - 1;

    while ((*_end < 65 || *_end > 90) && (*_end < 97 || *_end > 122) && (*_end < 48 || *_end > 57))
    {
        _end--;
    }
    *(_end + 1) = '\0';
    return ptr;
}