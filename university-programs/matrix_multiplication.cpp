#include <iostream>
#include <cstring>

/*
    Tarokh Yaghoubi , (jacob)
    Multiply Matrices . (They dont have the same size);

*/

using namespace std;

void multiply(int first[][4], int second[][6], int final[][6]);

int main()
{

    int i = 0, j = 0;
    int first_user_input = 0;
    int second_user_input = 0;

    int first_matrix[3][4] = { '0' };
    int second_matrix[4][6] = { '0' };
   // int final_matrix[3][6] = { '0' };
    int final_matrix[3][6];

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            cout << "Enter the value for this index : " << "[" << i << "]" << "[" << j << "]" << " ";
            cin >> first_matrix[i][j];
        }            
    }
    cout << "\n SECOND MATRIX IS COMING : ... \n\n";

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)
        {
            cout << "Enter the value for this index : " << "[" << i << "]" << "[" << j << "]" << " ";
            cin >> second_matrix[i][j];
        }
    }

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)        
            cout << first_matrix[i][j] << " ";
        cout << "\n";
    }
    cout << "\n";
        
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 6; j++)        
            cout << second_matrix[i][j] << " ";
        cout << "\n";
    }

    multiply(first_matrix, second_matrix, final_matrix);


    system("pause");
    return 0;
}

void multiply(int first[][4], int second[][6], int final[][6])
{

    int i, j;
    cout << endl;

    for(i = 0; i < 3; i++) {
        for(j = 0; j < 6; j++) {
            final[i][j] = 0;
            for(int k = 0; k < 4; k++) {
                final[i][j] += first[i][k] * second[k][j];
            }
        }
    }

    cout << "\n";
    for (i = 0; i < 3; i++)
    {
        for(j = 0; j < 6; j++)
            cout << final[i][j] << " ";
    
        cout << "\n";
    }

}
