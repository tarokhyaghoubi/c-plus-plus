#include <iostream>
#include <ctype.h>
#include <cstring>
#include <array>


using namespace std;

int binary_search(int arr[], int, int);

int main(int argc, char* argv[])
{

    // Write your code here 
    int result = 0;
    int inp = 10;
    int tmp;

    // I just defined a sorted array for test 
    // you can disorder the array if you want 

    int arr[] = { 2, 3, 4, 10, 40 };
    int n = sizeof(arr) / sizeof(arr[0]);

    // Bubble Sort Begins 

    int i, j;
    for (i = 0; i < n; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (arr[j] < arr[i])
            { 
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }

    // Take the final result 
    int x = 10;
    result = binary_search(arr, x, n);
    (result == -1)
        ? cout << "Element is not present in array"
        : cout << "Element is present at index " << result << endl;

    system("pause");
    return 0;

}

int binary_search(int arr[], int user_guess, int size)
{
     // i used a controller for handling True, false 

    int low, high, mid;
    low = high = mid = 0;

    high = size - 1;

    // middle of the array


    while (low <= high)
    {
        mid = low + (high - low) / 2;
        // Check if the middle index and the user guess are equals 

        if (arr[mid] == user_guess)
        {
            return mid;
        }
        else if (user_guess > arr[mid])
        {
            low = mid + 1;
        }
        else
        {
            high = mid - 1;

        }

    }   


    return -1;

}