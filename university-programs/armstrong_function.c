/*

	Tarokh (Jacob) Yaghoubi , a function to return 1 
	if the number is armstrong , return 0 , if the number 
	is not armstrong ,

*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>


int is_armstrong(int);

int main(int argc, char* argv[])
{
	int n = 0;
	printf("Enter a number : ");
	scanf("%d", &n);

	if (is_armstrong(n) == 1)
		printf("It is armstrong \n");
	else
		printf("It is not armstrong \n");

	return 0;
}

int is_armstrong(int n)
{
	int controller;

	int var = 0;
	int remainder = 0, sum = 0;
	int count = 0;
	var = n;

	while (var > 0)
	{
		remainder = var % 10;
		sum = sum + (remainder * remainder * remainder);
		var = var / 10;
	}
	if (sum == n)
		controller = 1;
	else
		controller = 0;

	return controller;

}