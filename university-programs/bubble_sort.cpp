/*

	Tarokh (Jacob) Yaghoubi . Sort an array in C++
	Sort the numbers from small to big .
    Bubble Sort .

*/

#include <iostream>
#include <ctype.h>
#include <cstring>

void printArray(int arr[], int);

int main(int argc, char* argv[])
{

    int tmp;
    
    int arr[] = { 5, 6, 4, 8, 9, 2, 3, 10, 12, 13, 15, 16 };
    int n = sizeof(arr) / sizeof(arr[0]);
    int i, j;
    
    for (i = 0; i < n; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (arr[j] < arr[i])
            {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }

    printArray(arr, n);
    return 0;
}

void printArray(int arr[], int n)
{
    int i;
    for (i = 0; i < n; i++)
        std::cout << ' ' << arr[i];
    
    std::cout << "\n";
}

