/* 

	Tarokh (Jacob) Yaghoubi , A function that 
	returns 1 if the number passed to it is armstrong 
	and it will return 0 if not , 

*/

#include <iostream>
#include <ctype.h>
#include <cstring>

using namespace std;

int is_armstrong(int);

int main(int argc, char* argv[])
{
	int n = 0;
	cout << "Enter a number : ";
	cin >> n;

	if (is_armstrong(n) == 1)
		cout << "It is armstrong \n";
	else
		cout << "It is not armstrong \n";

	system("pause");
	return 0;
}

int is_armstrong(int n)
{
	int controller;

	int var = 0;
	int remainder = 0, sum = 0;
	int count = 0;
	var = n;

	while (var > 0)
	{
		remainder = var % 10;
		sum = sum + (remainder * remainder * remainder);
		var = var / 10;
	}
	if (sum == n)
		controller = 1;
	else
		controller = 0;

	return controller;

}
