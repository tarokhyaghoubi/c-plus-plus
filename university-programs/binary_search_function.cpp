/*

    Tarokh (Jacob) Yaghoubi , Binary Search Function

*/

#include <iostream>
#include <ctype.h>
#include <cstring>

using namespace std;

int main()
{
    int user_guess = 2;
    int guess = 0;
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int low, high , mid;
    low = high = mid = 0;

    high = sizeof(arr) / sizeof(arr[0]);

    while (low <= high)
    {
            mid = high + low;
            guess = arr[mid];
            if (user_guess == guess)
            {
                cout << mid << endl;
            }
            if (guess > user_guess)
            {
                high = mid - 1;
            }                  
            else
            {
                low = mid + 1;
            }
    }
    
    return 0;

}