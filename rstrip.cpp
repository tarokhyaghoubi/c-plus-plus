/*

    Tarokh (Jacob) Yaghoubi , Remove White-Space from right-side
    of a character array in C++ , using a pointer to start from the end 
    of the array and seek for a character which is not (white space) .

*/

#include <iostream>
#include <cstring>
#include <ctype.h>

#include <string>

using namespace std;

char* rstrip(char* ptr);

int main(int argc, char* argv[])
{
    string text = "tarokh";
    char name[] = "My name is Tarokh , I'm a Programmer :)   ";
    cout << "The value of name is : " << "{" << name << "}" << endl;
    cout << "The new value of name is : " << "{" << rstrip(name) << "}" << endl;

    
    return 0; 

}

char* rstrip(char* ptr)
{
    char* _end;
    int size = strlen(ptr);
    _end = (size + ptr) - 1;

    while (_end >= ptr && isspace(*_end))
        _end--;

    *(_end + 1) = '\0';

    return ptr;
}