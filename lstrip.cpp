#include <iostream>
#include <ctype.h>
#include <cstring>

/*
    
    Tarokh (Jacob) Yaghoubi , A program to Recognize 
    the White-Space on the left side of a Character-Array
    and use memmove to remove the whitespace .

*/

using namespace std;

int l_strip(char* ptr);

int main(int argc, char* argv[])
{
    char name[] = "   My name is tarokh , I am a programmer :)";
    cout << "The string is :" << name << endl;
    int len = strlen(name);
    int lstrip_index = l_strip(name);
    memmove(&name[0], &name[lstrip_index], (len - lstrip_index));
    name[len - lstrip_index] = '\0';
    cout << "The string is :" << name << endl;
    system("pause");
    return 0;

}

int l_strip(char* ptr)
{
    int i = 0;
    while (*ptr && isspace(*ptr))
    {
        ptr++;
        i++;
    }

    return i;
}