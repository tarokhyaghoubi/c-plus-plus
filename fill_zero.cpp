/*

	Tarokh (Jacob) Yaghoubi , Fill zero from left 
	second version . (test)

*/

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctype.h>
#include <cstring>

using namespace std;

int main()
{
    int len, max = 12;
    int new_len = 0;
    char number[12] = "123";
    char zero[13] = "000000000000";

    len = strlen(&number[0]);
    new_len = max - len;
    strcpy(&zero[new_len], &number[0]);
    zero[12] = '\0';

    for (int i = 0; i < sizeof(zero)/sizeof(zero[0]); i++)
        cout << zero[i];

    cout << "\n";
    system("pause");
    return 0;
}
